import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac043 on 2017/5/1.
  */
object SquareApp extends App{
  val conf=new SparkConf().setAppName("Square").setMaster("local[*]")

  val sc=new SparkContext(conf)

  val intRdd=sc.parallelize(1 to 1000)

  val squares=intRdd.map(x=>x*x)

  squares.foreach(println)  //所有平方
  squares.collect().foreach(println)
  squares.take(10).foreach(println)   //取１０個平方

}