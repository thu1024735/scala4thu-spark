import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac043 on 2017/5/1.
  */
object StrLengthsApp extends App{
  val conf=new SparkConf().setAppName("StrLength").setMaster("local[*]")

  val sc=new SparkContext(conf)

  val nums=sc.textFile("numbers.txt")

  val strLength: RDD[Int] =nums.map(_.length)
//  println(strLength.collect().toList)

  strLength.foreach(len=>println(len))

  // 計算字數 //

}
